#!/bin/sh

currentDate=`date`
name=$1

mkdir "$name at $currentDate"
cd "$name at $currentDate"
mkdir about_me
mkdir my_friends
mkdir my_system_info

cd about_me
mkdir personal
mkdir professional

cd personal
echo "https://www.facebook.com/$2" > facebook.txt
cd ../professional
echo "https://www.linkedin.com/in/$3" > linkedin.txt

cd ../../my_system_info
echo "My username: `uname`" > about_this_laptop.txt
echo "With host: `uname -a`" >> about_this_laptop.txt
echo "`ping -c 3 www.google.com`" > internet_connection.txt

cd ../my_friends
curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt -o list_of_my_friends.txt
